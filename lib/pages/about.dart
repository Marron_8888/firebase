import 'package:flutter/material.dart';

class About extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("About"),
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              sbox(),
              stdGroup(
                assetImage: AssetImage('assets/images/ya.jpg'),
                stdID: '6250110021',
                stdName: 'นางสาวอภิญญา กล้าศึก',
              ),
              sbox(),
              stdGroup(
                assetImage: AssetImage('assets/images/kim.jpg'),
                stdID: '6250110024',
                stdName: 'นางสาวปรมาภรณ์ ชูหนู',
              ),
              sbox(),
              stdGroup(
                assetImage: AssetImage('assets/images/bim.png'),
                stdID: '6250114019',
                stdName: 'นางสาวเบญจนารี นวลนิ่ม',
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class sbox extends StatelessWidget {
  const sbox({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 24,
    );
  }
}

class stdGroup extends StatelessWidget {
  const stdGroup({
    this.assetImage,
    this.stdID,
    this.stdName,
    Key key,
  }) : super(key: key);

  final AssetImage assetImage;
  final String stdID;
  final String stdName;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 32),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        color: Colors.cyan,
      ),
      padding: EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CircleAvatar(
                radius: 30,
                backgroundImage: assetImage,
              ),
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Text(
            stdID,
            style: TextStyle(
                fontSize: 18,
                color: Colors.white,
                fontWeight: FontWeight.w700,
                letterSpacing: 1.0),
          ),
          SizedBox(
            height: 7,
          ),
          Text(
            stdName,
            style: TextStyle(
                fontSize: 18,
                color: Colors.white,
                fontWeight: FontWeight.w600,
                letterSpacing: 1.0),
          ),
        ],
      ),
    );
  }
}
